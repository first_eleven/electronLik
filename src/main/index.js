import { app, BrowserWindow } from 'electron'

const  { globalShortcut, Menu } = require('electron');
// import {
//   Menu,
//   protocol,
//   BrowserWindow,
//   ipcMain,
//   Tray, // 托盘
// } from 'electron'
/**
 * Set `__static` path to static files in production
 * https://simulatedgreg.gitbooks.io/electron-vue/content/en/using-static-assets.html
 */
if (process.env.NODE_ENV !== 'development') {
  global.__static = require('path').join(__dirname, '/static').replace(/\\/g, '\\\\')
}

let mainWindow
const winURL = process.env.NODE_ENV === 'development'
  ? `http://localhost:9080`
  : `file://${__dirname}/index.html`

function createWindow () {
  /**
   * Initial window options
   */
  mainWindow = new BrowserWindow({
    height: 980,
    useContentSize: true,
    width: 1913,
    frame:true
    // webPreferences:{
    //   devTools:true   //隐藏调试工具
    // }
  })
  mainWindow.loadURL(winURL)
  mainWindow.on('closed', () => {
    mainWindow = null
  })
}


const Menus = [
  {
    label:'菜单',
    submenu:[
      {
        label: '网页版',
        role: 'help',
        submenu: [{
          label: '网页版',
          click: function () {
            // electron.shell.openExternal('https://www.jianshu.com/u/1699a0673cfe')
            console.log("网页版");
          }
        }]
      },
      {
        label: '帮助',
        role: 'help',
        submenu: [{
          label: '帮助文档',
          click: function () {
            // electron.shell.openExternal('https://www.jianshu.com/u/1699a0673cfe')
              console.log("帮助文档");
          }
        }]
      }
    ]
  },
    {
        label:'编辑',
        submenu:[
            {
                label: '网页版',
                role: 'help',
            },
            {
                label: '帮助',
                role: 'help',
            }
        ]
    },{
    label: '帮助',
        submenu:[{
                label: '退出'
        }]
    }
]


app.on('ready', createWindow)
//我这里是放到createWindow函数里面的
const mainMenu = Menu.buildFromTemplate(Menus);
Menu.setApplicationMenu(mainMenu);

app.on('window-all-closed', () => {
  if (process.platform !== 'darwin') {
    app.quit()
  }
})

app.on('activate', () => {
  if (mainWindow === null) {
    createWindow()
  }
})

/**
 * Auto Updater
 *
 * Uncomment the following code below and install `electron-updater` to
 * support auto updating. Code Signing with a valid certificate is required.
 * https://simulatedgreg.gitbooks.io/electron-vue/content/en/using-electron-builder.html#auto-updating
 */


/*
import { autoUpdater } from 'electron-updater'

autoUpdater.on('update-downloaded', () => {
  autoUpdater.quitAndInstall()
})

app.on('ready', () => {
  if (process.env.NODE_ENV === 'production') autoUpdater.checkForUpdates()
})
 */
